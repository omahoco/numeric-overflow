'use strict';

const MAXINT = 2147483647;
const MININT = 0;

class Amount  {
    constructor(value) {
        const amount = Number(value)
        if(Number.isNaN(amount) || amount < MININT || amount > MAXINT) {
            throw new RangeError();
        } 

      this.value = amount;
    }

    add(other) {
        if (this.value + other.value > MAXINT) {
            throw new RangeError();
        }
    
        return new Amount(this.value + other.value);    
    }

    greaterThan(other) {
       return this.value > other.value; 
    }
  }

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();
    // configurations
    app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => {
    if (approval(parseInt(req.query.amount))) {
        res.status(400).end(req.query.amount + ' requires approval.');
    } else {
        res.status(200).end(req.query.amount + ' does not require approval.');
    }
});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {
    const amt = new Amount(value);
    const threshold = new Amount(1000);
    const surcharge = new Amount(10);

    const totalAmount = amt.add(surcharge);
    
    return totalAmount.greaterThan(threshold);
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function () {
    process.exit();
});

module.exports = { app, approval };
